# GitOps implementation example with GitlabCI

Example project implementing **GitOps deployment** for a simple static website on S3 bucket:

## Usage

Deployment consist of a CloudFormation stack managing S3 bucket and Route53 records (using existing Hosted Zone).

Setup:

- Create or use an AWS IAM user with enough permissions to manage deployment (S3 bucket, Route53 records and CloudFormation stack)
  - See [`iam-policy.json`](./iam-policy.json) for minimal required permissions
- Create or use some Hosted Zone for our DNS records, such as `devops.crafteo.io.`
- Configure GitLabCI variables with AWS credentials for our IAM user, for example:
  ```
  AWS_ACCESS_KEY_ID: AKIAXXX
  AWS_DEFAULT_REGION: eu-west-3
  AWS_SECRET_ACCESS_KEY: <secret>
  ```

GitOps implementation:

- Pushing on any branch (except `master`) will deploy a dev website at `http://gitlabci-gitops-s3-website.$CI_COMMIT_REF_SLUG.devops.crafteo.io`
  - Example en `dev` branch: `http://gitlabci-gitops-s3-website.dev.devops.crafteo.io`
  - [GitLab review app](https://docs.gitlab.com/ee/ci/review_apps/) with stop job will ba available as well (auto stop after 1 day)
- Pushing/merging on `master` will deploy website at `http://gitlabci-gitops-s3-website.devops.crafteo.io`
- See [`.gitlab-ci.yml`](./.gitlab-ci.yml) for details

Manual deployment:

```
# Deploy CF stack
# Replace stack name, Hosted Zone and FQDN with your desired domain names
aws cloudformation deploy --template-file cloudformation.yml \
    --stack-name my-website-stack \
    --parameter-overrides \
    HostedZoneName='devops.crafteo.io.' \
    WebsiteFQDN='gitlabci-gitops-s3-website.devops.crafteo.io'

# Cleanup
# Delete CloudFormation stack, but bucket will be kept as we use Retained deletion policy
aws cloudformation delete-stack --stack-name my-website-stack
# Cleanup remaining bucket (won't be deleted with stack)
aws s3 rb s3://gitlabci-gitops-s3-website.devops.crafteo.io --force
```
